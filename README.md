# Projet EIG 3 (dont le titre est à déterminer)


## Résumé

Ce projet porte sur le développement d'un pipeline coordonnant les outils nécessaires pour les différentes étapes menant à l'ouverture de données expérimentales issues de la recherche (1) avec le moins de friction possible et (2) permettant une analyse plus poussée des jeux de données.

Ce pipeline sera appliqué dès les premières itération à l'ouverture de données certains jeux de données de recherche.

## Contexte

La mise à disposition en open data des données de recherche va amener à des frictions (fichiers perdus de vus dans un arbre de ficrhiers, absence de normalisation de l'association d'un jeu de données à un projet)

et le niveau d'exigence (seulement mettre en ligne) risque d'amener à une faible réutilisabilité de ces données (du fait de l'absence de contexte, d'alignement de schémas de données).

Par ailleurs, le processus d'ouverture de données peut également bénéficier à l'auteur·e des données.

## Objectif visé

Les objectifs visés sont :
- de rendre les étapes que doit réaliser l'auteur·e d'un jeu de données pour répondre à l'exigence de mise à disposition en open data soit la plus simple possible
- de permettre la contextualisation des données au travers des schémas de données
- de permettre des analyses de données et visualisations permises par (i) la contextualisation des données et par (ii) la mise en commun de jeux de données similaires.

## Approche choisie

De nombreux outils répondant à des étapes de ce pipeline existent et peuvent être associés. Cela exige de gérer astucieusement la communication entre ces outils ainsi que le développement d'une interface permettant à l'utilisateur de préparer des jeux de données, réaliser des analyses et les passer en open data en un clic.

Le développement d'une telle plateforme sera nourri par les retours d'expérience d'utilisateurs beta travaillant avec des jeux de données de recherche.

## Calendrier

- Dépôt du projet : 15 juin 2018
- défense du projet (10 min de présentation) : début juillet
- annonce des projets retenus : mi-juillet
- recrutement des EIG : septembre-novembre
- contrats EIG : janvier-octobre 2019

[Cahier des charges](https://entrepreneur-interet-general.etalab.gouv.fr/assets/20180504-AAP-EIG3.pdf)
[Fiche à fournir](https://gitlab.com/jibe-b/eig-open-research-data/tree/master/AAPEIG3_Fichedetaillee.odt)
[diapos (12 pages)](https://gitlab.com/jibe-b/eig-open-research-data/tree/master/diapos.md)

## Profils EIG

(profil et expertises recherchées : mission )

- 1 développeu·r·se (full-stack) : développement d'un pipeline d'outils existants ou à prototype et d'une webapp
- 1 data scientist (data mining, visualisation) : minage de bases de données existantes pour identifier des jeux de données, mise en place des outils d'analyse et visualisation de données
- (optionnel) 1 UX designer : optimisation du parcours utilisateur pour résoudre tout point de friction

---

À définir en commun : 

## Problématique à résoudre (<120 mots)

## Solution proposée (<120 mots)

## Bénéfices et impacts attendus en termes de transformation numérique

## Types d'innovations mobilisées (<40mots)

## État d’avancement du défi (pas démarré/phase pilote, technologies et méthodes utilisées) (<40mots)

---

Premières pistes d'action :

## Éléments d'action envisagés

Data mining :
- minage des jeux de données contenant des indications sur les données ayant été produites dans les dernières X années (en se basant sur les travaux des EIG [scanR](https://scanr.enseignementsup-recherche.gouv.fr/) et [dataESR](https://entrepreneur-interet-general.etalab.gouv.fr/defi/2017/09/26/dataesr/))

Développements :
- adaptation des briques logiciels du défi EIG [Archifiltre](http://archifiltre.com/) pour le minage des disques durs et serveurs ayant servi pour le stockage de données expérimentales
- mise en place d'un pipeline de stockage des jeux de données identifiés avec versionnement
- développement d'outils de contextualisation, linking des schémas de données (en lien avec [wikidata](https://wikidata.org))
- développement d'une interface de visualisation et d'analyse des données aggrégées 
- tests A/B de la webapp afin d'optimiser le parcours utilisateur

Accompagnement à l'ouverture de quelques jeux de données :
- crowdsourcing d'indications d'existence de jeux de données
- contextualisation et analyse de jeux de données en amont de l'ouverture
- mise à disposition en open data de jeux de données
- data mining sur des jeux de données mis en open data

## Portage du projet

Portage envisagé :

portage opérationnel :
- 1 établissement public ayant une mission de valorisation de données existantes
- 1 établissement public ayant des compétences de valorisation de données

portage administratif :
- en convention avec le ministère de l'Enseignement Supérieur, de la Recherche et de l'Innovation
- en partenariat avec une université engagée dans l'ouverture des données de la recherche

## Points forts

Ce projet vise à **simplifier le processus d'ouverture des données de la recherche**, en accord avec la loi pour une République numérique et vise à améliorer la qualité des données ouvertes afin de les rendre réellement exploitables.

Le pipeline vise à **permettre aux auteur·e·s des données d'être les premièr·e·s bénéficiaires du processus d'ouverture de données**. En effet, l'aggrégation des données permet de faire exécuter des analyses n'ayant pas été réalisées en amont, ainsi que la visualisation de ces jeux de données. Fournir des outils d'analyse et de visualisation au travers de la webapp permet également d'abaisser la marche en termes de compétences nécessaires.


## Critères à respecter
- être dimensionné pour être réalisable en 10 mois - état actuel : discutable, concentrer le champ du projet
- dimensionné pour 2 à 3 EIG : ok pour 2 EIG, éventuellement 3
- nécessite des expertises numériques : ok


## À mettre en place au niveau des établissements 

- 2 mentors (allouant chacun 30% de son temps de travail) (1 mentor opérationel, 1 mentor stratégique)
- 1 équipe pluridisciplinaire associée au projet

## Exemples de jeux de données concernées par ce projet

- exemple de données expérimentales  

## Actions prévues pour la pérennisation du projet après la fin du défi

- plan de déploiment au sein des établissements publics concernés de la solution développée





